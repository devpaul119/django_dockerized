from __future__ import absolute_import
from django.conf import settings


if settings.APP_ENV == 'development':
    PORT = ""
    # NOT = ""

elif settings.APP_ENV == 'production':
    PORT = ":8081"
